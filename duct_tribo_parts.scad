
h = 90; // or lenght of the duct
width = 2;
diameter = 12.5; // barrel pipe internal diameter, substract about 0.5 for loosen fit

duct_half();
translate([0,20,0]) rotate([180,0,0]) duct_half(1);
//translate([0,0,0.1]) duct_half(1);


module duct_half(part = 0)
{	
	z_offset = (part == 0) ? -3 : 3;
	intersection(){
		translate([-1,0,z_offset]) sin_duct(part = part);
		rotate([0,90,0]) cylinder (r=diameter/2, h = h);
	}
	// intake support 
	scale([1,1,(part == 0) ? 1 : -1])
	difference(){
	intersection(){
		cube([15,diameter+3,10], center = true); 
		rotate([0,90,0]) cylinder (r=diameter/2+1.5, h = 2);
	}
		translate([-1,-8,0]) cube([5,16,10]);
		translate([-1,-5,-3]) cube([5,10,3]);
	}
	// distances
	if(part == 0){
		for(i = [0:24:h]){
		for(m = [diameter/2-1.5,-diameter/2+0.5])
			translate([4+i,m,0]) cube([2,1,3]);
		}
	}
}

module sin_duct(duct_width = diameter, part = 0){
	// sin duct settings
	amplitude = 2;
	frequency = 15;
	points_no = h+2;
	
	width = (part == 0) ? width : -width;
	// calculate [ x, y ] for one point
	function point(i, R, r) = (i%2 ==0) ? [
		i, 
		//sin(i*frequency)*amplitude + width*pow(-1,i)
		sin(i*frequency)*amplitude + width
		//if(i%2 !=0)	0;
	] : [i,0-width]; 

	points = [ for(i = [0:1:points_no]) point(i) ]; 
	paths = [ for (i = [0:2:points_no]) [i, i+2,i+3,i+1] ]; 


	rotate([90,0,0])  linear_extrude(height = duct_width, center = true)
	polygon(points, paths=paths); 
}