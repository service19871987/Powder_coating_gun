//how many sides the bottle will have
resolution = 48;//[12,24,36,48,96]

//external diameter of bottle neck cylinder (for cap)
opening_diameter = 36.5;//[16:70]

//external diameter of bottle neck thread (with thread coil - for cap)
thread_diameter = 39;

// thread jump mm per one rotation
thread_jump = 3;

// external diameter of air supply pipe, adjust it to fit your setup
supply_pipe_dia = 14+0.3;

// discharge pipe dia, default is for PVC pipe
discharge_pipe_dia = 16+0.3;

// lenghts of pipe connection nests
supply_pipe_len = 35;
discharge_pipe_len = 10;

//neck finish, this decides what caps can fit
threads = 9; 

// diameter of hole for HV cable
cable_dia = 5;

$fn= resolution;
neck_radius = opening_diameter/2;
discharge_pipe_dia_ext = discharge_pipe_dia+3;
supply_pipe_dia_ext = supply_pipe_dia+6;


difference()
{
union(){
translate([0,0,10])
cap(3);
cap_base();
//pipe
translate([0,-7,4.5]) {
// in bottle air pipe
cylinder(r=5, h= 40);
cylinder(r=4.1, h= 50);
}
}
	// funnels
	translate([0,-7,4.5]) cylinder(r=3, h= supply_pipe_dia_ext+40);
	translate([-4,-5,supply_pipe_dia_ext-supply_pipe_dia-1.5]) rotate([90,0,0]) cube([8,8,opening_diameter]);

	translate([0,7,3.5]) cylinder(r=6, h= discharge_pipe_dia_ext);
	translate([-5,5,discharge_pipe_dia_ext-discharge_pipe_dia+0.5])scale([1,-1,1]) rotate([90,0,0]) cube([10,10,opening_diameter]);
	// hv cable funnel, coordinates are just hardcoded
	translate([21,-10,(discharge_pipe_dia_ext/2)-3]) rotate([-90,0,28]) 
		cylinder(r=cable_dia/2, h= 50, $fn=8);
}




module cap_base()
{
	difference()
{
hull(){
	cylinder(r2=thread_diameter/2+1.5, r1=thread_diameter/3, h = 10);
	
	translate([0,-thread_diameter/2, (supply_pipe_dia_ext)/2]) rotate([90,0,0])
	cylinder(r=(supply_pipe_dia_ext)/2, h= supply_pipe_len+4);


	translate([0,thread_diameter/2, (discharge_pipe_dia_ext)/2]) rotate([-90,0,0]) 
 	cylinder(r=(discharge_pipe_dia_ext)/2, h= discharge_pipe_len+4);
}
// holes
	union(){
	translate([0,0,10]) cylinder(r = thread_diameter/2, h = 20);
	
	translate([0,-(thread_diameter/2)-4, (supply_pipe_dia_ext)/2]) rotate([90,0,0])
	cylinder(r=(supply_pipe_dia)/2, h= supply_pipe_len+1);

	translate([0,(thread_diameter/2)+4, (discharge_pipe_dia_ext)/2]) rotate([-90,0,0])
	cylinder(r=(discharge_pipe_dia)/2, h= discharge_pipe_len+1);

	}
}
}

module pipe(h, d, t = 5)
{ 
	linear_extrude(height = h){
	difference()
	{
		circle(d=d + t );
		circle(d=d);
	}
	}
}

module cap(extra_h=0)
{
	difference(){
	linear_extrude(height = 15+extra_h){
		circle(r=opening_diameter/2 + 3.0);
		
		for(i=[0:30:360])
		{
			rotate([0,0,i]) 
			translate([opening_diameter/2+3,0,0])
				circle(r = 2);
		};
	}

	union(){
		translate([0,0,4+extra_h]){
			thread(thread_jump, thread_diameter, 9,10);
			cylinder(r=opening_diameter/2, h=16.5);
			translate([0,0,-2.5]) 
				cylinder(r=thread_diameter/2, h=3);
		}
	}
}
}

module screwthread_triangle(P) {
	difference() {
		translate([-sqrt(3)/2.7*P+sqrt(3)/2*P/8,0,0])
		rotate([90,0,0])
		cylinder(r=sqrt(3)/2.0*P,h=0.00001,$fn=3,center=true);

		translate([0,-P/2,-P/2])
		cube([P,P,P]);
	}
}
module screwthread_onerotation(P,D_maj,step) {
	H = sqrt(3)/2*P;
	D_min = D_maj - 5*sqrt(3)/8*P;

	for(i=[0:step:360-step])
	hull()
		for(j = [0,step])
		rotate([0,0,(i+j)])
		translate([D_maj/2,0,(i+j)/360*P])
		screwthread_triangle(P);
}
module thread(P,D,h,step) {
	for(i=[0:h/P])
	translate([0,0,i*P])
	screwthread_onerotation(P,D,step);
}







